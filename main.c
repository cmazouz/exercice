#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int plgd2(int x, int y)
{if (x > y)
    {return x;}
else
{return y;}
}

int plgd3 (int x, int y, int z)
{
    if (x<y)
    {
        if ( y<z){
            return z;}
        else {return y;}
    }
    else
    {
        if (x<z)
        {
            return z;
        }
        else
        {
            return x;
        }
    }
}
int plgd4(int a, int b, int c, int d)
{
    if (plgd2(a, b)<plgd2(c, d))
    {
        return plgd2(c, d);
    }
    else
    {
        return plgd2(a, b);
    }
}
int plgd5(int a, int b, int c, int d, int e)
{
    if (plgd3(a, b, c)< plgd2(d, e))
    {
        return plgd2(d, e);
    }
    else
    {
        return plgd3(a, b, c);
    }
}
int sec2(int a,int b)
{
    if (a<b)
    {
        return a;
    }
    else
    {
        return b;
    }
}
int sec3(int a, int b, int c)
{
    if (sec2(a, b)<sec2(b, c))
    {
        return sec2(b, c);
    }
    else
    {
        return sec2(a, b);
    }
}
int sec4(int a, int b, int c, int d)
{
    if (plgd2(a, b)<plgd2(c, d))
    {
        return plgd2(a, b);
    }
    else
    {
        return plgd2(c, d);
    }
}
int puissance(int n,int p)
{
    if (p==0)
    {
        return 1;
    }
    else
    {
        return n * puissance(n, (p-1));
    }
}
int sompn(int n,int p)
{
    if (n==0)
    {
        return 0;
    }
    else
    {
        return puissance(n, p)+sompn((n-1), p);
    }
}
int main()
{
    printf("3 est plus petit que %d\n", plgd2(5, 3));
    printf("le plus grand nombre parmis (11, 150, 120) est %d\n", plgd3(11, 150, 120));
    printf("Le plus grand nompbre parmis (100, 160, 1, 300) est %d\n", plgd4(100, 160, 1, 300));
    printf("Le plus grand nombre parmis (100, 160, 300, 1, 10) est %d\n", plgd5(100, 160, 300, 1, 10));
    printf("Le second plus grand nombre parmis (150, 130, 115) est %d\n", sec3(150, 130, 115));
    printf("Le second plus grand nombre parmis (1100, 2000, 3500, 450) est %d\n", sec4(1100, 2000, 3500, 450));
    printf("La somme pn de 2 a la puissance 4 est %d\n ", sompn(2, 4));
    return 0;
}
