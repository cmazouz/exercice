#include <stdio.h>

int som(int n)
{
    if (n == 0)
    {
        return 0;
    }
    else
    {
        return n + som(n-1);
    }
}
int som2(int n)
{
    if (n == 0)
    {
        return 0;
    }
    else
    {
        return (n*n)+som2(n-1);
    }
}
int som3(int n)
{
    if (n == 0)
    {
        return 0;
    }
    else
    {
        return (n*n*n)+som3(n-1);
    }
}
int som4(int n)
{
    if (n == 0)
    {
        return 0;
    }
    else
    {
        return (n*n*n*n)+som4(n-1);
    }
}
int prd(int n)
{
    if (n == 0)
    {
        return 1;
    }
    else
    {
        return n*prd(n-1);
    }

}
int prd2(int n)
{
    if (n == 0)
    {
        return 1;
    }
    else
    {
        return (n*n)*prd2(n-1);
    }
}
int prd3(int n)
{
    if (n == 0)
    {
        return 1;
    }
    else
    {
        return (n*n*n)*prd3(n-1);
    }
}
int prd4(int n)
{
    if (n == 0)
    {
        return 1;
    }
    else
    {
        return (n*n*n*n)*prd4(n-1);
    }
}
int puissance(int n,int  p)
{
    if (p==0)
    {
        return 1;
    }
    else
    {
        return n * puissance(n, (p-1));
    }
}
int sompn(int n,int p)
{
    if (n==0)
    {
        return 0;
    }
    else
    {
        return puissance(n, p)+sompn((n-1), p);
    }
}
int add (int a,int b)
{
    if (b == 0)
    {
        return 0;
    }
    else
    {
        return a+1 + add(0, (b-1));
    }


}
int soustraction(int a,int b)
{
    if (b==0)
    {
        return 0;
    }
    else
    {
        return a-1 + soustraction(0, (b-1));
    }
}
int multiplication_russe(int a,int b)
{
    if (b==0)
    {
        return 0;
    }
    else
    {
        return a + multiplication_russe(a, (b-1));
    }
}
int dv(int a,int b)
{
    if (a<b)
    {
        return 0;
    }
    else
    {
        return 1 + dv((a-b), b);
    }
}
int reste_division(int a,int b)
{
    if (a<b)
    {
        return 0;
    }
    else
    {
        return a-((dv(a, b))*b);
    }
}
int r(int a,int n)
{
    if (n*n >= a)
    {
        return n;
    }
    else
    {
        return r(a, (n+1));
    }
}
int main()
{
    printf("la somme de 5 est %d\n", som(5));
    printf("la somme carre de 3 est %d\n", som2(3));
    printf("la somme cube de 3 est %d\n", som3(3));
    printf("la somme des puissances 4 de 3 est %d\n", som4(3));
    printf("le produit de 3 est %d\n", prd(3));
    printf("Le produit au carre de 3 est%d\n", prd2(3));
    printf("le produit cube de 3 est %d\n", prd3(3));
    printf("le produit a la puissance 4 de 3 est %d\n", prd4(3));
    printf("4 puissance 3 est egal a %d\n", puissance(4, 3));
    printf("La sommepn de 2 a la puissance 4 est %d\n ", sompn(2, 4));
    printf("l'addition par incrementation successive de 3 et 7 est egale a %d\n", add(3, 7));
    printf("le resultat de l'operation 10-4 est %d\n", soustraction(10, 4));
    printf("le resultat de la multiplication de 6 par 7 est %d\n", multiplication_russe(6, 7));
    printf("le resultat de la division de 9 par 2 est %d\n", dv(9, 2));
    printf("le reste de la division de 9 par 2 est%d\n", reste_division(9, 2));
    printf("la racine carre de 9 est %d", r(9, 0));
    return 0;
}
